# Create Virtual environment:
      virtualenv -p python3 env
  
# Activate virtual environment:
      source env/bin/activate

# Run and access the application locally:
      python run.py  (or)
      heroku local web  # If you have installed heroku locally

      ### Access appication
      Access application on http://localhost:5000

# Running the application as a Docker: pushing to heroku
      
      docker build -t python-heroku:latest .
      docker run -d -p 5000:5000 cherrypy-heroku
      Access the application on http://localhost:5000

# Push Docker image to heroku
      heroku container:login
      heroku create <PROVIDE APP NAME HERE>
      heroku container:push web --app <PROVIDE APP NAME HERE>
      heroku container:release web --app <PROVIDE APP NAME HERE>
      Access the application on https://<APP NAME HERE>.herokuapp.com
